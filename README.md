Visualize parsing tree in penn tree bank format.

# Demo
[http://annotation.dbi.udel.edu/~ligang/penn-tree-viz/readTree.html](http://annotation.dbi.udel.edu/~ligang/penn-tree-viz/readTree.html)

# Example parsing tree 
    (S
      (NP (PRP I))
      (VP (VBP have)
        (NP (DT a) (NN book)))
      (. .))

# JS library
[d3.js](http://d3js.org/)

[dagre-d3](https://github.com/cpettitt/dagre-d3)